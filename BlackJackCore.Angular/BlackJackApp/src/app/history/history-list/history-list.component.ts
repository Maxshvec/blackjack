import { Component, OnInit } from '@angular/core'
import { StartService } from 'src/app/start/start.service';
import { GetHistoryGameView } from '../../shared/model/getHistoryGameView';


@Component({
    selector: 'app-history-list',
    templateUrl: './history-list.component.html'
})

export class HistoryListComponent implements OnInit {

    public gameHistory: GetHistoryGameView;

    public done: boolean = false;

    constructor(private htttpService: StartService) {

    }

    ngOnInit() {

        this.htttpService.getHistory()
            .subscribe(
                response => {
                    this.gameHistory = response;
                    this.done = true;
                },
                error => {
                    console.log(error);
                    this.done = false;
                }
            )

    }
}
