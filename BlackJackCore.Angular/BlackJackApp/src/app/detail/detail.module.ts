import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { DetailRoutingModule } from './detail-routing.module';
import { DetailListComponent } from './detail-list/detail-list.component';
import { GridModule } from '@progress/kendo-angular-grid';

@NgModule({
    imports: [
        CommonModule,
        DetailRoutingModule,
        GridModule
    ],
    declarations: [DetailListComponent]
})

export class DetailModule { }
