import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GameDeskComponent } from './game-desk/game-desk.component';

const routes: Routes = [
    {
        path: '',
        component: GameDeskComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class GameRoutingModule { }