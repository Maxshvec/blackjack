export class ResponseEnoughGameView {

    constructor(

        public gamePlayers: Array<GamePlayerEnoughGameViewItem>
    ) { }
}

export class GamePlayerEnoughGameViewItem {

    public playerId: string;
    public name: string;
    public score: number;
}