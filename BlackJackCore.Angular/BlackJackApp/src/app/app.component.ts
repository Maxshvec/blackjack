import { Component } from '@angular/core';
import { StartService } from './start.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [StartService]
})
export class AppComponent {

  title = "Black Jack";

  constructor(private router: Router) {

  }

  getHistory() {
    this.router.navigate(['/history']);
  }
}