import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnoughRoutingModule } from './enough-routing.module';
import { EnoughListComponent } from './enough-list/enough-list.component';

@NgModule({
    imports: [
        CommonModule,
        EnoughRoutingModule
    ],
    declarations: [EnoughListComponent]
})

export class EnoughModule { }
