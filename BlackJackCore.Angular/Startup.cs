﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BlackJackCore.Angular
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            
            string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;Database=blackJackdb;Integrated Security=True";

            //services.AddDbContext<DataContext>(options => options.UseSqlServer(connectionString));
            //services.AddTransient<IGameService, GameService>();
            //services.AddTransient<IGameLogic, GameLogicService>();
            //services.AddTransient<IPlayerLogic, PlayerLogicService>();
            //services.AddTransient<IPlayerRepository, PlayerRepository>();
            //services.AddTransient<IRoundRepository, RoundRepository>();
            //services.AddTransient<IGamePlayerRepository, GamePlayerRepository>();
            //services.AddTransient<IWinnerRepository, WinnerRepository>();
            //services.AddTransient<IGameHistoryRepository, GameHistoryRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                //routes.MapRoute(
                //    name: "api",
                //    template: "api/{controller}/{action}/{id?}");
            });
        }
    }
}
