﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BlackJackCore.Angular.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        //IGameService _gameService;

        //public GameController(IGameService gameService)
        //{
        //    _gameService = gameService;
        //}

        //[HttpGet]
        //public async Task<IActionResult> GetUsers()
        //{
        //    List<PlayerGetUserGameViewItem> names = await _gameService.GetPlayer();

        //    return Ok(names);
        //}

        // GET: api/Game
        //[HttpGet]
        //public async Task<IActionResult> StartAsync(string name)
        //{
            
        //}

        // GET: api/Game/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Game
        [HttpPost]
        [ActionName("StartGame")]
        public void StartGame([FromBody] string value)
        {
        }

        // PUT: api/Game/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
