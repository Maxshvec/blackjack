﻿using BlackJack.Logic.Infrastucture;
using BlackJack.Logic.Interfaces;
using BlackJack.ViewModels.ViewModels;
using BlackJack.ViewModels.ViewModels.GameController;
using BlackJack.ViewModels.ViewModels.GameController.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BlackJack.WEB.Controllers
{
    public class GameController : Controller
    {
        IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> CreateUser(string name, int bot)
        {
            Guid gameId = new Guid();

            try
            {
                gameId = await _gameService.StartGameAsync(name, bot);
            }

            catch (ValidationException ex)
            {
                return Content(ex.Message);
            }
            
            return RedirectToAction("Game", "Game", new { gameId });
        }

        [HttpGet]
        public async Task<ActionResult> Game(Guid id)
        {
            List<RoundGetGameGameViewItem> rounds = await _gameService.GetGameCard(id);

            GetGameGameView listRound = new GetGameGameView
            {
                Rounds = rounds
            };

            return View(listRound);
        }

        [HttpGet]
        public async Task<ActionResult> Take(Guid Id)
        {
            bool result = await _gameService.ContinueGameAsync(Id);

            if (result == false)
            {
                return RedirectToAction("Enough", "Game", new { Id });
            }

            ResponseTakeGameView currentRound = await _gameService.TakeDistributionAsync(Id);

            return RedirectToAction("Game", "Game", new { Id });
        }

        [HttpGet]
        public async Task<ActionResult> Enough(Guid Id)
        {
            List<GamePlayerEnoughGameViewItem> winners = await _gameService.CompleteGame(Id);

            ResponseEnoughGameView listGamePlayers = new ResponseEnoughGameView
            {
                GamePlayers = winners
            };

            return View(listGamePlayers);
        }

        [HttpGet]
        public ActionResult GameHistories()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> GameDetails(Guid Id)
        {
            GameDetailsGameView game = await _gameService.GetGameById(Id);
            
            return View(game);
        }

        //[HttpGet]
        //public async Task<JsonResult> GetGameDetails(string Id)
        //{
        //    List<RoundGetGameDetailsViewItem> gameDetails = await _gameService.GetGameDetailsAsync(new Guid(Id));

        //    return Json(gameDetails, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        public async Task<JsonResult> GetHistories()
        {
            List<GameHistoryGetGameHistoriesGameViewItem> gameHistoryViewModels = await _gameService.GetGameHistoriesAsync();

            return Json(gameHistoryViewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetUser()
        {
            List<PlayerGetUserGameViewItem> names = await _gameService.GetPlayer();

            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Test()
        {
            return View();
        }
    }
}