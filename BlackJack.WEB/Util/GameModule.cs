﻿using BlackJack.Logic.Interfaces;
using BlackJack.Logic.Services;
using Ninject.Modules;

namespace BlackJack.WEB.Util
{
    public class GameModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGameService>().To<GameService>();
        }
    }
}