﻿$(document).ready(function () {
    $("#grid").kendoGrid({
        dataSource: {
            type: "json",
            transport: {
                //read: "@Url.Action(" + GetHistories + ", " + Game + ")"
                read: '/Game/GetHistories'
            },
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        Name: { type: "string" },
                        CountPlayer: { type: "number" },
                        Id: { type: "string" }
                    }
                }
            },
            pageSize: 20
        },
        height: 550,
        groupable: true,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [{
            field: "Name",
            title: "Name",
            width: 240
        }, {
            field: "CountPlayer",
            title: "Count Player",
            width: 240
        }, {
            template: function (dataItem) {
                return '<a class=k-button href=/Game/GetGameDetails/' + dataItem.id + '> Details</a>';
            },
            field: "Id"
        }
        ]
    });
});