﻿using BlackJack.Logic.Infrastucture;
using BlackJack.Logic.Util;
using BlackJack.WEB.Util;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BlackJack.WEB
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // внедрение зависимостей
            NinjectModule gameModule = new GameModule();
            NinjectModule serviceModule = new ServiceModule("Connection");
            var kernel = new StandardKernel(gameModule, serviceModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));

            AutoMapperConfiguration.Configure();
        }
    }
}
