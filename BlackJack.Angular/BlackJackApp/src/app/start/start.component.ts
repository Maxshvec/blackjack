import { Component, OnInit } from '@angular/core';
import { StartService } from './start.service';
import { Router } from '@angular/router';
import { PlayerViewModel } from '../shared/model/playerViewModel';
import { RequestStartGameView } from '../shared/model/requestStartGameView';
import { RequestCreateUserGameView } from '../shared/model/requestCreateUserGameView';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css'],
  providers: [StartService]
})
export class StartComponent implements OnInit {

  public id: string;
  public filter: string;
  
  public requestStartGameView : RequestStartGameView;
  public requestCreateUserGameView: RequestCreateUserGameView;

  public users: PlayerViewModel[];
  public data: Array<PlayerViewModel>;
  public listBots: Array<number> = [1, 2, 3, 4, 5];

  constructor(private router: Router, private httpService: StartService) {

    this.requestStartGameView = new RequestStartGameView;
    this.requestCreateUserGameView = new RequestCreateUserGameView;
   }

  ngOnInit() {

    this.httpService.getUsers()
      .subscribe(response => {
        this.users = response;
        this.data = response;
      },
        error => {
          console.log(error);
        }
      )
  }

  public addNew(): void {
    
    this.requestCreateUserGameView.name = this.filter;
    this.httpService.createUser(this.requestCreateUserGameView).subscribe(
      response => {
        // console.log(this.users);
        this.users.push(response);
        this.data.push(response);
        this.handleFilter(this.filter);
      },
      error => {
        console.log(error);
      });
    
  }

  public handleFilter(value) {

    this.filter = value;
    this.data = this.users.filter((s) => s.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  public submit(name: string, numberBots: number) {

    this.requestStartGameView.name = name;
    this.requestStartGameView.numberBots = numberBots;
    
    this.httpService.startGame(this.requestStartGameView)
      .subscribe(response => {
        this.id = response;
        this.router.navigate(['/game', this.id]);
      },
        error => {
          console.log(error);
        }
      )
  }
}
