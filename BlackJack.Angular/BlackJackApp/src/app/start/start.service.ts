import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlayerViewModel } from '../shared/model/playerViewModel';
import { RequestStartGameView } from '../shared/model/requestStartGameView';
import { RequestCreateUserGameView } from '../shared/model/requestCreateUserGameView';
import { environment } from 'src/environments/environment';
import { RequestTakeGameView } from '../shared/model/requestTakeGameView';
import { RequestEnoughGameView } from '../shared/model/requestEnoughGameView';
import { ResponseEnoughGameView } from '../shared/model/responseEnoughGameView';
import { GetGameGameView, RoundGetGameGameViewItem } from '../shared/model/getGameGameView';
import { GetHistoryGameView } from '../shared/model/getHistoryGameView';
import { GetGameDetailsGameView } from '../shared/model/getGameDetailsGameView';

@Injectable({
  providedIn: 'root'
})

export class StartService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  public createUser(model: RequestCreateUserGameView): Observable<RequestCreateUserGameView> {

    return this.http.post<RequestCreateUserGameView>(this.baseUrl + 'Game/CreateUser', model);
  }

  public getUsers(): Observable<PlayerViewModel[]> {

    return this.http.get<PlayerViewModel[]>(this.baseUrl + 'Game/GetUsers');
  }

  public startGame(model: RequestStartGameView): Observable<string> {
    
    return this.http.post<string>(this.baseUrl + 'Game/StartGame', model);
  }

  public getGame(id: string): Observable<GetGameGameView> {

    return this.http.get<GetGameGameView>(this.baseUrl + 'Game/GetGame?id=' + id);
  }

  public take(model: RequestTakeGameView): Observable<RoundGetGameGameViewItem> {

    return this.http.post<RoundGetGameGameViewItem>(this.baseUrl + 'Game/Take', model);
  }
  
  public enough(model: RequestEnoughGameView): Observable<ResponseEnoughGameView> {

    return this.http.post<ResponseEnoughGameView>(this.baseUrl + 'Game/Enough', model);
  }

  public getHistory(): Observable<GetHistoryGameView> {

    return this.http.get<GetHistoryGameView>(this.baseUrl + 'Game/GetHistory');
  }

  public getDetail(id: string): Observable<GetGameDetailsGameView> {

    return this.http.get<GetGameDetailsGameView>(this.baseUrl + 'Game/GetDetail?id='+ id);
  }
}
