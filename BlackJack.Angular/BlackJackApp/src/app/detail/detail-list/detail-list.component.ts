import { Component, OnInit } from '@angular/core';
import { StartService } from 'src/app/start/start.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GetGameDetailsGameView } from '../../shared/model/getGameDetailsGameView';

@Component({
    selector: 'app-detail-list',
    templateUrl: './detail-list.component.html'
})

export class DetailListComponent implements OnInit {

    id: string;
    done: boolean = false;
    gameDetails: GetGameDetailsGameView;

    constructor(private httpService: StartService, private activateRoute: ActivatedRoute, private router: Router) {

        this.id = activateRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this.httpService.getDetail(this.id)
        .subscribe(
            response => {
                this.gameDetails = response;
                this.done = true;
            },
            error => {
                console.log(error);
                this.done = false;
            }
        )
    }

    continue() {
        this.router.navigate(['/game', this.id]);
    }
}