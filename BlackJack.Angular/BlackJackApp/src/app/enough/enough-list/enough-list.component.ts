import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StartService } from 'src/app/start/start.service';
import { RequestEnoughGameView } from '../../shared/model/requestEnoughGameView';
import { ResponseEnoughGameView } from '../../shared/model/responseEnoughGameView';

@Component({
    selector: 'app-enough-list',
    templateUrl: './enough-list.component.html'
})

export class EnoughListComponent implements OnInit {
    
    public id: string;
    public done: boolean = false;
    requestEnoughGame: RequestEnoughGameView;
    responseEnoughGame: ResponseEnoughGameView;

    constructor(private httpService: StartService, private activateRoure: ActivatedRoute){

        this.id = activateRoure.snapshot.params['id'];
        this.requestEnoughGame = new RequestEnoughGameView();
    }

    ngOnInit() {

        if (this.id) {
            this.loadEnough();
        }
    }

    private loadEnough() {

        this.requestEnoughGame.id = this.id;
        this.httpService.enough(this.requestEnoughGame)
        .subscribe(
            response => {
                this.responseEnoughGame = response;
                this.done = true;
            },
            error => {
                console.log(error);
                this.done = false;
            }
        )
    }

}