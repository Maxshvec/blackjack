import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnoughListComponent } from './enough-list/enough-list.component';


const routes: Routes = [
    {
        path: '',
        component: EnoughListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class EnoughRoutingModule { }
