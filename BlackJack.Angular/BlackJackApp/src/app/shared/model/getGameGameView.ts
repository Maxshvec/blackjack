import { GameStatus } from './enum/gameStatus';

export class GetGameGameView {
    constructor(

        public rounds: Array<RoundGetGameGameViewItem>
    ) { }
}

export class RoundGetGameGameViewItem {

    public roundNumber: number;
    public gameStatus: GameStatus;

    constructor(

        public gamePlayers: Array<GamePlayerGetGameGameViewItem>
    ) { }
}

export class GamePlayerGetGameGameViewItem {

    public playerId: string;
    public name: string;
    public isUser: boolean;
    public rankCard: string;
    public suitCard: number;
    public suit: string;
    public score: number;
}
