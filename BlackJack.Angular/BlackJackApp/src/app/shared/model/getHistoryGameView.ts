export class GetHistoryGameView {

    constructor(

        public gameHistories: Array<GameHistoryGetGameHistoriesGameViewItem>
    ) { }
}

export class GameHistoryGetGameHistoriesGameViewItem {

    public id: string;
    public countPlayer: number;
    public name: string;
}