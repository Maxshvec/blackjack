import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StartService } from '../../start/start.service';
import { RequestTakeGameView } from '../../shared/model/requestTakeGameView';
import { GetGameGameView } from '../../shared/model/getGameGameView';
import { GameStatus } from 'src/app/shared/model/enum/gameStatus';

@Component({
  selector: 'app-game-desk',
  templateUrl: './game-desk.component.html',
  styleUrls: ['./game-desk.component.css']
})
export class GameDeskComponent implements OnInit {
  
  id: string;
  listRoundViewModel: GetGameGameView;
  done: boolean = false;

  score: Array<{playerName: string, score: number}> = [];
  userScore: Array<{playerName: string, score: number}> = [];
  
  requestTakeGameView: RequestTakeGameView;
  max: number = 0;
  constructor(private router: Router, private httpService: StartService, private activateRoute: ActivatedRoute) {

    this.id = activateRoute.snapshot.params['id'];
    this.requestTakeGameView = new RequestTakeGameView();
  }

  ngOnInit() {

    if (this.id) {
      this.loadGame();
    }
  }

  private loadGame() {

    this.httpService.getGame(this.id)
      .subscribe(
        response => {
          this.listRoundViewModel = response;
          response.rounds.forEach(element => this.max = element.roundNumber)
          
          response.rounds.forEach(element => {
            if (element.roundNumber == this.max) {
              this.CountScore(element);
            }
          })
          this.done = true;
          this.listRoundViewModel.rounds.reverse();
        },
        error => {
          console.log(error);
          this.done = false;
        }
      )
  }

  public take() {

    this.requestTakeGameView.id = this.id;
    this.httpService.take(this.requestTakeGameView)
      .subscribe(
        response => {
          if(response.gameStatus == GameStatus.InProcess) {
            this.listRoundViewModel.rounds.unshift(response);
            this.CountScore(response);
          }
          if(response.gameStatus == GameStatus.Finished) {
            this.enough();
          }
        },
        error => {
          console.log(error);
        }
      )
  }

  private CountScore(element: any) {
    this.score = [];
    this.userScore = [];
    element.gamePlayers.forEach(item => {
      if (item.isUser == true) {
        this.userScore.push({ playerName: item.name, score: item.score });
      }
      else {
        this.score.push({ playerName: item.name, score: item.score })
      }
    })
  }

  public enough() {

    this.router.navigate(['/enough', this.id]);
  }
}