import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameRoutingModule } from './game-routing.module'
import { GameDeskComponent } from './game-desk/game-desk.component'

@NgModule({
    imports: [
        CommonModule,
        GameRoutingModule
    ],
    declarations: [GameDeskComponent]
})

export class GameModule { }