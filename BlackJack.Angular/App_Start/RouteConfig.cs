﻿using BlackJack.Angular.App_Start;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace BlackJack.Angular
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new
                {
                    serverRoute = new ServerRouteConstraint(url =>
                    {
                        return url.PathAndQuery.StartsWith("/Settings", StringComparison.InvariantCultureIgnoreCase);
                    })
                });

            routes.MapRoute(
            name: "angular",
            url: "{*url}",
            defaults: new { controller = "Home", action = "Index" } // The view that bootstraps Angular 2
            );
        }
    }
}
