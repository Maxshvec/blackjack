﻿using Autofac;
using Autofac.Integration.WebApi;
using BlackJack.Data.EF;
using BlackJack.Data.Interfaces;
using BlackJack.Data.Interfaces.Repositories;
using BlackJack.Data.Repositories;
using BlackJack.Logic.Card;
using BlackJack.Logic.Interfaces;
using BlackJack.Logic.Services;
using BlackJack.Logic.Util;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BlackJack.Angular
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            HttpConfiguration config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<GameService>().As<IGameService>();

            builder.RegisterType<GameLogicService>().As<IGameLogic>();
            builder.RegisterType<PlayerLogicService>().As<IPlayerLogic>();
            
            builder.RegisterType<PlayerRepository>().As<IPlayerRepository>();
            builder.RegisterType<RoundRepository>().As<IRoundRepository>();
            builder.RegisterType<GamePlayerRepository>().As<IGamePlayerRepository>();
            builder.RegisterType<WinnerRepository>().As<IWinnerRepository>();
            builder.RegisterType<GameHistoryRepository>().As<IGameHistoryRepository>();

            builder.RegisterType<DataContext>().As<IDataContext>();

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            builder.RegisterWebApiModelBinderProvider();

            // Set the dependency resolver to be Autofac.
            IContainer container = builder.Build();
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            HttpConfiguration httpConfig = GlobalConfiguration.Configuration;
            httpConfig.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            httpConfig.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            AutoMapperConfiguration.Configure();
        }
    }
}
