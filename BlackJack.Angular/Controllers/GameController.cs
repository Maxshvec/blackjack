﻿using BlackJack.Logic.Infrastucture;
using BlackJack.Logic.Interfaces;
using BlackJack.ViewModels.ViewModels.GameController;
using BlackJack.ViewModels.ViewModels.GameController.Request;
using BlackJack.ViewModels.ViewModels.GameController.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlackJack.Angular.Controllers
{
    public class GameController : ApiController
    {
        IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetUsers()
        {
            List<PlayerGetUserGameViewItem> names = await _gameService.GetPlayer();

            return Ok(names);
        }

        [HttpPost]
        public async Task<IHttpActionResult> StartGame([FromBody]RequestStartGameView request)
        {
            Guid gameId = new Guid();

            try
            {
                gameId = await _gameService.StartGameAsync(request.Name, request.NumberBots);
            }

            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(gameId);
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateUser([FromBody]RequestCreateUserGameView user)
        {
            try
            {
                await _gameService.CreateUser(user.name);
            }

            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(user);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetGame(string id)
        {
            GetGameGameView listRound = new GetGameGameView();

            try
            {
                Guid gameId = new Guid(id);

                List<RoundGetGameGameViewItem> rounds = await _gameService.GetGameCard(gameId);

                listRound.Rounds = rounds;
            }

            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(listRound);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Take([FromBody]RequestTakeGameView request)
        {
            ResponseTakeGameView currentRound = new ResponseTakeGameView();

            try
            {
                Guid gameId = new Guid(request.Id);
                
                currentRound = await _gameService.TakeDistributionAsync(gameId);
            }

            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            
            return Ok(currentRound);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Enough([FromBody]RequestEnoughGameView request)
        {
            ResponseEnoughGameView response = new ResponseEnoughGameView();

            try
            {
                Guid id = new Guid(request.Id);

                List<GamePlayerEnoughGameViewItem> winners = await _gameService.CompleteGame(id);

                response.GamePlayers = winners;
            }

            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(response);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetHistory()
        {
            GetHistoryGameView listGameHistory = new GetHistoryGameView();

            try
            {
                List<GameHistoryGetGameHistoriesGameViewItem> gameHistoryViewModels = await _gameService.GetGameHistoriesAsync();

                listGameHistory.GameHistories = gameHistoryViewModels;
            }

            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(listGameHistory);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetDetail(string id)
        {
            GetGameDetailsGameView response = new GetGameDetailsGameView();

            try
            {
                response = await _gameService.GetGameDetailsAsync(new Guid(id));
            }

            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(response);
        }

        //// PUT: api/Game/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Game/5
        //public void Delete(int id)
        //{
        //}
    }

}
