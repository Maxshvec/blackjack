﻿using BlackJack.Core.Entities;
using System.Data.Entity;

namespace BlackJack.Data.EF
{
    public class DataContext : DbContext
    {
        public DataContext(string connectionString) : base(connectionString)
        {
        }

        public DataContext()
        {
        }

        public DbSet<Player> Players { get; set; }

        public DbSet<GamePlayer> GamePlayers { get; set; }

        public DbSet<Winner> Winners { get; set; }

        public DbSet<GameHistory> GameHistories { get; set; }

        public DbSet<Round> Rounds { get; set; }
    }
}