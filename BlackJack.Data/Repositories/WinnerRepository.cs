﻿using BlackJack.Core.Entities;
using BlackJack.Data.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.Data.Repositories
{
    public class WinnerRepository : BaseRepository<Winner>, IWinnerRepository
    {
        public WinnerRepository() : base("Winners")
        {
        }

        public async Task<List<Winner>> GetWinnersByGameIdAsync(Guid gameId)
        {
            string where = @"WHERE GameHistoryId = @gameId";

            IEnumerable<Winner> winners = await QueryAsync(where, param: new { gameId });

            List<Winner> winnersList = winners.ToList();

            return winnersList;
        }
    }
}
