﻿using BlackJack.Core.Entities;
using BlackJack.Data.Interfaces.Repositories;
using BlackJack.Entitie.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Repositories
{
    public class GamePlayerRepository : BaseRepository<GamePlayer>, IGamePlayerRepository
    {
        public GamePlayerRepository() : base("GamePlayers") { }

        public async Task<GamePlayer> GetGamePlayerByRoundIdAsync(Guid playerId, Guid roundId)
        {
            string where = @"WHERE PlayerId = @playerId AND RoundId = @roundId";

            GamePlayer gamePlayer = await QueryFirstOrDefaultAsync(where, param: new { playerId, roundId });

            return gamePlayer;
        }

        public async Task<IEnumerable<GamePlayer>> GetPlayersInGameByRoundIdAsync(Guid roundId)
        {
            int status = (int)Status.InGame;
            
            string where = @"WHERE RoundId = @roundId AND Status = @status";

            IEnumerable<GamePlayer> gamePlayer = await QueryAsync(where, param: new { roundId, status });

            return gamePlayer;
        }
        
        public async Task<IEnumerable<GamePlayer>> GetWinnersAsync(Guid roundId)
        {
            int status = (int)Status.Win;
            
            string where = @"WHERE RoundId = @roundId AND Status = @status";

            IEnumerable<GamePlayer> gamePlayers = await QueryAsync(where, param: new { roundId, status });

            return gamePlayers;
        }
        
        public async Task<IEnumerable<GamePlayer>> GetGamePlayersInLastRoundByGame(Guid gameId)
        {
            int status = (int)Status.InGame;

            string where = @"where RoundId in (select Id from Rounds where GameHistoryId = @gameId and RoundNumber = (select MAX(RoundNumber)
											from Rounds where GameHistoryId = @gameId)) and Status = 0";

            IEnumerable<GamePlayer> gamePlayers = await QueryAsync(where, param: new { gameId, status });

            return gamePlayers;
        }

        public async Task<GamePlayer> GetUserByGameInLastRound(Guid gameId)
        {
            int status = (int)Status.InGame;

            int playerStatus = (int)TypePlayers.Player;
            
            string where = @"where RoundId = (select Id from Rounds where RoundNumber = 
						(select MAX(RoundNumber) from Rounds where GameHistoryId = @gameId) and GameHistoryId = @gameId) and PlayerId in 
                                                                (select Id from Players where TypePlayerId = @playerStatus) and Status = @status";

            GamePlayer player = await QueryFirstOrDefaultAsync(where, param: new { gameId, playerStatus, status });

            return player;
        }
    }
}
