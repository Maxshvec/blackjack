﻿using BlackJack.Core.Entities;
using BlackJack.Data.Interfaces.Repositories;
using BlackJack.Entitie.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Repositories
{
    public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
    {
        public PlayerRepository() : base("Players")
        {
        }

        public async Task<IEnumerable<Player>> GetBotsAsync(int amount)
        {
            int status = (int)TypePlayers.Bot;

            string where = @"WHERE TypePlayerId = @status ORDER BY Name OFFSET 0 ROWS FETCH NEXT @amount ROWS ONLY";

            IEnumerable<Player> players = await QueryAsync(where, param: new { status, amount });

            return players;
        }

        public async Task<Player> GetDillerAsync()
        {
            int status = (int)TypePlayers.Diller;

            string where = @"WHERE TypePlayerId = @status";

            Player player = await QueryFirstOrDefaultAsync(where, param: new { status });

            return player;
        }
        
        public async Task<Player> GetUserByIdAsync(Guid playerId)
        {
            int status = (int)TypePlayers.Player;

            string where = @"WHERE Id = @playerId AND TypePlayerId = @status";

            Player player = await QueryFirstOrDefaultAsync(where, param: new { playerId, status });

            return player;
        }

        public async Task<IEnumerable<Player>> GetPlayersAsync()
        {
            int status = (int)TypePlayers.Player;

            string where = @"WHERE TypePlayerId = @status";

            IEnumerable<Player> players = await QueryAsync(where, param: new { status });

            return players;
        }

        public async Task<Player> FindPlayerByName(string name)
        {
            int status = (int)TypePlayers.Player;

            string where = @"WHERE TypePlayerId = @status AND Name = @name";

            Player player = await QueryFirstOrDefaultAsync(where, param: new { status, name });

            return player;
        }

        public async Task<Player> GetPlayerById(Guid playerId)
        {
            string where = @"WHERE Id = @playerId";

            Player player = await QueryFirstOrDefaultAsync(where, param: new { playerId });

            return player;
        }

        public async Task<Player> GetUserInGame(Guid gameId)
        {
            int typePlayer = (int)TypePlayers.Player;

            string where = @"where Id in (select PlayerId from GamePlayers where RoundId = 
                                            (select Id from Rounds where GameHistoryId = @gameId and RoundNumber = 0)) 
                            and TypePlayerId = @typePlayer";

            Player player = await QueryFirstOrDefaultAsync(where, param: new { gameId, typePlayer });

            return player;
        }

    }
}
