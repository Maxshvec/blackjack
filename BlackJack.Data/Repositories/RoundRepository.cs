﻿using BlackJack.Core.Entities;
using BlackJack.Data.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Linq;

namespace BlackJack.Data.Repositories
{
    public class RoundRepository : BaseRepository<Round>, IRoundRepository
    {
        public RoundRepository() : base("Rounds")
        {
        }

        public async Task<Round> GetPreviousRoundByGameIdAsync(Guid gameId)
        {
            string where = @"WHERE GameHistoryId = @gameId 
                            AND RoundNumber = (SELECT MAX(RoundNumber) -1 FROM Rounds where GameHistoryId = @gameId)";

            Round round = await QueryFirstOrDefaultAsync(where, param: new { gameId });

            return round;
        }

        public async Task<Round> GetLastRoundByGameIdAsync(Guid gameId)
        {
            string where = @"WHERE GameHistoryId = @gameId AND RoundNumber = (SELECT MAX(RoundNumber) 
                            FROM Rounds where GameHistoryId = @gameId)";

            Round round = await QueryFirstOrDefaultAsync(where, param: new { gameId });

            return round;
        }

        public async Task<List<Round>> GetRoundsByGameIdAsync(Guid gameId)
        {
            string query = @"SELECT * FROM Rounds JOIN GamePlayers ON GamePlayers.RoundId = Rounds.Id WHERE GameHistoryId = @gameId";

            using (IDbConnection db = Connection)
            {
                var roundDictionary = new Dictionary<Guid, Round>();

                IEnumerable<Round> list = await db.QueryAsync<Round, GamePlayer, Round>(
                    query,
                    (round, gamePlayer) =>
                    {
                        if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
                        {
                            roundEntry = round;
                            roundEntry.GamePlayers = new List<GamePlayer>();
                            roundDictionary.Add(roundEntry.Id, roundEntry);
                        }

                        roundEntry.GamePlayers.Add(gamePlayer);
                        return roundEntry;
                    },
                    param: new { gameId },
                    splitOn: "Id");

                List<Round> roundsList = list
                    .Distinct()
                    .OrderBy(s => s.RoundNumber)
                    .ToList();

                return roundsList;
            }
        }

        //public async Task<Round> GetLastRoundByGameIdWithPlayersAsync(Guid gameId)
        //{
        //    int status = (int)Status.InGame;

        //    string query = @"select * from Rounds 
        //                JOIN GamePlayers ON GamePlayers.RoundId = Rounds.Id 
        //                WHERE GameHistoryId = @gameId AND RoundNumber = (SELECT MAX(RoundNumber) 
        //                FROM Rounds where GameHistoryId = @gameId) AND Status = @status";

        //    using (IDbConnection db = Connection)
        //    {
        //        var roundDictionary = new Dictionary<Guid, Round>();

        //        IEnumerable<Round> list = await db.QueryAsync<Round, GamePlayer, Round>(
        //            query,
        //            (round, gamePlayer) =>
        //            {
        //                if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
        //                {
        //                    roundEntry = round;
        //                    roundEntry.GamePlayers = new List<GamePlayer>();
        //                    roundDictionary.Add(roundEntry.Id, roundEntry);
        //                }

        //                roundEntry.GamePlayers.Add(gamePlayer);
        //                return roundEntry;
        //            },
        //            param: new { gameId, status },
        //            splitOn: "Id");

        //        Round result = list.Distinct()
        //            .FirstOrDefault();

        //        return result;
        //    }
        //}

        public async Task<List<Round>> GetGameDetails(Guid gameId)
        {
            //string query = @"SELECT Distinct * FROM Rounds 
			         //               JOIN GamePlayers ON GamePlayers.RoundId = Rounds.Id
			         //               LEFT OUTER JOIN Winners ON Winners.GameHistoryId = Rounds.GameHistoryId AND Winners.PlayerId = GamePlayers.PlayerId 
            //                WHERE Rounds.GameHistoryId = @gameId
            //                ORDER BY RoundNumber";
            string query = @"SELECT Distinct * FROM Rounds 
			                        JOIN GamePlayers ON GamePlayers.RoundId = Rounds.Id
                            WHERE Rounds.GameHistoryId = @gameId
                            ORDER BY RoundNumber";

            using (IDbConnection db = Connection)
            {
                var roundDictionary = new Dictionary<Guid, Round>();

                IEnumerable<Round> list = await db.QueryAsync<Round, GamePlayer, Round>(
                    query,
                    (round, gamePlayer) =>
                    {
                        if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
                        {
                            roundEntry = round;
                            roundEntry.GamePlayers = new List<GamePlayer>();

                            roundDictionary.Add(roundEntry.Id, roundEntry);
                        }
                        roundEntry.GamePlayers.Add(gamePlayer);

                        return roundEntry;
                    },
                    param: new { gameId },
                    splitOn: "Id");

                List<Round> result = list.Distinct()
                    .ToList();

                return result;
            }
        }
    }
}
