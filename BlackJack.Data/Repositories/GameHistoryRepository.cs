﻿using BlackJack.Core.Entities;
using BlackJack.Data.Interfaces.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.Data.Repositories
{
    public class GameHistoryRepository : BaseRepository<GameHistory>, IGameHistoryRepository
    {
        public GameHistoryRepository() : base("GameHistories")
        {
        }

        public async Task<List<GameHistory>> GetGameHistory()
        {
            string query = @"SELECT * FROM GameHistories 
		                            JOIN Players ON Players.Id IN (SELECT PlayerId FROM GamePlayers 
					                WHERE RoundId IN (SELECT Id FROM Rounds WHERE GameHistoryId = GameHistories.Id AND RoundNumber = 0)) WHERE TypePlayerId = 0";

            using (IDbConnection db = Connection)
            {
                var gameHistoryDictionary = new Dictionary<Guid, GameHistory>();

                IEnumerable<GameHistory> gameHistories = await db.QueryAsync<GameHistory, Player, GameHistory>(
                    query,
                    (game, player) =>
                    {
                        if (!gameHistoryDictionary.TryGetValue(game.Id, out GameHistory gameEntry))
                        {
                            gameEntry = game;
                            gameEntry.Players = new List<Player>();
                            gameHistoryDictionary.Add(gameEntry.Id, gameEntry);
                        }

                        gameEntry.Players.Add(player);

                        return gameEntry;
                    },
                    splitOn: "Id");

                List<GameHistory> gameList = gameHistories.ToList();

                return gameList;
            }
        }
    }
}
