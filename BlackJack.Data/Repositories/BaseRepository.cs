﻿using BlackJack.Core.Entities.Base;
using BlackJack.Data.Helpers;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BlackJack.Data.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : EntityBase
    {
        private readonly string _tableName;

        private string _connectionString = null;

        internal IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
                //return new SqlConnection(_connectionString);
            }
        }

        public BaseRepository(string tableName)
        {
            _tableName = tableName;
        }

        public async Task<IEnumerable<T>> GetAllAsync(T entity)
        {
            using (IDbConnection db = Connection)
            {
                return await db.GetAllAsync<T>();
            }
        }

        public async Task<T> GetAsync(Guid id)
        {
            using (IDbConnection db = Connection)
            {
                return await db.GetAsync<T>(id);
            }
        }

        public async Task<Guid> AddAsync(T entity)
        {
            using (IDbConnection db = Connection)
            {
                Guid result = db.Insert<Guid, T>(entity);

                return result;
            };
        }

        public async Task<bool> DeleteAsync(T entity)
        {
            using (IDbConnection db = Connection)
            {
                return await db.DeleteAsync(entity);
            }
        }

        public async Task<IEnumerable<T>> QueryAsync(string where, object param)
        {
            string query = $@"SELECT * FROM { _tableName } { where }";

            using (IDbConnection db = Connection)
            {
                return await db.QueryAsync<T>(query, param: param);
            }
        }

        public async Task<T> QueryFirstOrDefaultAsync(string where, object param)
        {
            string query = $@"SELECT * FROM { _tableName + " " + where }";

            using (IDbConnection db = Connection)
            {
                var result = await db.QueryFirstOrDefaultAsync<T>(query, param: param);

                return result;
            }
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            using (IDbConnection db = Connection)
            {
                return await db.UpdateAsync(entity);
            }
        }
    }
}
