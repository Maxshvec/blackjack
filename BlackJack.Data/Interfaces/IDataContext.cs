﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace BlackJack.Data.Interfaces
{
    public interface IDataContext
    {
        Database Database { get; }

        DbContextConfiguration Configuration { get; }

        DbChangeTracker ChangeTracker { get; }

        Task<int> SaveChangesAsync();

        DbEntityEntry<T> Entry<T>(T entity) where T : class;

        DbSet<T> Set<T>() where T : class;
    }
}
