﻿using BlackJack.Core.Entities;
using BlackJack.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Interfaces.Repositories
{
    public interface IWinnerRepository : IBaseRepository<Winner>
    {
        Task<List<Winner>> GetWinnersByGameIdAsync(Guid gameId);
    }
}
