﻿using BlackJack.Core.Entities;
using BlackJack.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Interfaces.Repositories
{
    public interface IPlayerRepository : IBaseRepository<Player>
    {
        Task<IEnumerable<Player>> GetBotsAsync(int amount);

        Task<Player> GetDillerAsync();
        
        Task<IEnumerable<Player>> GetPlayersAsync();

        Task<Player> GetUserByIdAsync(Guid playerId);

        Task<Player> FindPlayerByName(string name);

        Task<Player> GetPlayerById(Guid playerId);

        Task<Player> GetUserInGame(Guid gameId);
    }
}
