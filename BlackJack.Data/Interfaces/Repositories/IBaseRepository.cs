﻿using BlackJack.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Repositories
{
    public interface IBaseRepository<T> where T : EntityBase
    {
        Task<Guid> AddAsync(T entity);

        Task<bool> DeleteAsync(T entity);

        Task<IEnumerable<T>> QueryAsync(string where, object param);

        Task<T> QueryFirstOrDefaultAsync(string where, object param);

        Task<bool> UpdateAsync(T entity);

        Task<T> GetAsync(Guid id);

        Task<IEnumerable<T>> GetAllAsync(T entity);
    }
}
