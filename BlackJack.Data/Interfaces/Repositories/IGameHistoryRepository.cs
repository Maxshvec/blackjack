﻿using BlackJack.Core.Entities;
using BlackJack.Data.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Interfaces.Repositories
{
    public interface IGameHistoryRepository : IBaseRepository<GameHistory>
    {
        Task<List<GameHistory>> GetGameHistory();
    }
}
