﻿using BlackJack.Core.Entities;
using BlackJack.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Interfaces.Repositories
{
    public interface IGamePlayerRepository : IBaseRepository<GamePlayer>
    {
        Task<GamePlayer> GetGamePlayerByRoundIdAsync(Guid playerId, Guid roundId);

        Task<IEnumerable<GamePlayer>> GetPlayersInGameByRoundIdAsync(Guid roundId);

        Task<IEnumerable<GamePlayer>> GetWinnersAsync(Guid roundId);

        Task<IEnumerable<GamePlayer>> GetGamePlayersInLastRoundByGame(Guid gameId);

        Task<GamePlayer> GetUserByGameInLastRound(Guid gameId);
    }
}
