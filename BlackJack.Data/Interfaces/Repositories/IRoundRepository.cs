﻿using BlackJack.Core.Entities;
using BlackJack.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Data.Interfaces.Repositories
{
    public interface IRoundRepository : IBaseRepository<Round>
    {
        Task<Round> GetPreviousRoundByGameIdAsync(Guid gameId);
        
        Task<List<Round>> GetRoundsByGameIdAsync(Guid gameId);

        Task<Round> GetLastRoundByGameIdAsync(Guid gameId);
        
        Task<List<Round>> GetGameDetails(Guid gameId);
    }
}
