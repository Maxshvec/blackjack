namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteWinnerIdPropertyFromGameHistoriesTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GameHistories", "WinnerId", "dbo.Players");
            DropIndex("dbo.GameHistories", new[] { "WinnerId" });
            DropColumn("dbo.GameHistories", "WinnerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GameHistories", "WinnerId", c => c.Guid());
            CreateIndex("dbo.GameHistories", "WinnerId");
            AddForeignKey("dbo.GameHistories", "WinnerId", "dbo.Players", "Id");
        }
    }
}
