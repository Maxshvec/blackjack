namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedNavigationPropertyToGamePlayersTable : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.GamePlayers", "RoundId");
            AddForeignKey("dbo.GamePlayers", "RoundId", "dbo.Rounds", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GamePlayers", "RoundId", "dbo.Rounds");
            DropIndex("dbo.GamePlayers", new[] { "RoundId" });
        }
    }
}
