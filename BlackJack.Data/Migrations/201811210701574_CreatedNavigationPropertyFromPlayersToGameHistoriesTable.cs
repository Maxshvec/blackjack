namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedNavigationPropertyFromPlayersToGameHistoriesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Players", "GameHistoryId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Players", "GameHistoryId");
        }
    }
}
