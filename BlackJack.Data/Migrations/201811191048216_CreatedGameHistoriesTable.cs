namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedGameHistoriesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GameHistories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CountPlayer = c.Int(nullable: false),
                        WinnerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.WinnerId, cascadeDelete: true)
                .Index(t => t.WinnerId);
            
            CreateTable(
                "dbo.Rounds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RoundNumber = c.Int(nullable: false),
                        GameHistoryId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GameHistories", t => t.GameHistoryId)
                .Index(t => t.GameHistoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GameHistories", "WinnerId", "dbo.Players");
            DropForeignKey("dbo.Rounds", "GameHistoryId", "dbo.GameHistories");
            DropIndex("dbo.Rounds", new[] { "GameHistoryId" });
            DropIndex("dbo.GameHistories", new[] { "WinnerId" });
            DropTable("dbo.Rounds");
            DropTable("dbo.GameHistories");
        }
    }
}
