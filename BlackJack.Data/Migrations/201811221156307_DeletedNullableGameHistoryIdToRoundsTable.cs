namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedNullableGameHistoryIdToRoundsTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rounds", "GameHistoryId", "dbo.GameHistories");
            DropIndex("dbo.Rounds", new[] { "GameHistoryId" });
            AlterColumn("dbo.Rounds", "GameHistoryId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Rounds", "GameHistoryId");
            AddForeignKey("dbo.Rounds", "GameHistoryId", "dbo.GameHistories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rounds", "GameHistoryId", "dbo.GameHistories");
            DropIndex("dbo.Rounds", new[] { "GameHistoryId" });
            AlterColumn("dbo.Rounds", "GameHistoryId", c => c.Guid());
            CreateIndex("dbo.Rounds", "GameHistoryId");
            AddForeignKey("dbo.Rounds", "GameHistoryId", "dbo.GameHistories", "Id");
        }
    }
}
