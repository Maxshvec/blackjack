namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNavigationPropertyToGamePlayersTable : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.GamePlayers", "PlayerId");
            AddForeignKey("dbo.GamePlayers", "PlayerId", "dbo.Players", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GamePlayers", "PlayerId", "dbo.Players");
            DropIndex("dbo.GamePlayers", new[] { "PlayerId" });
        }
    }
}
