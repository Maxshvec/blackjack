namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedGamePlayersTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GamePlayers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        RoundId = c.Guid(nullable: false),
                        RankCard = c.Int(nullable: false),
                        SuitCard = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GamePlayers");
        }
    }
}
