namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedWinnersTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Winners",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        GameHistoryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GameHistories", t => t.GameHistoryId, cascadeDelete: true)
                .Index(t => t.GameHistoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Winners", "GameHistoryId", "dbo.GameHistories");
            DropIndex("dbo.Winners", new[] { "GameHistoryId" });
            DropTable("dbo.Winners");
        }
    }
}
