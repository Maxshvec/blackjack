namespace BlackJack.Data.Migrations
{
    using BlackJack.Core.Entities;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<BlackJack.Data.EF.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BlackJack.Data.EF.DataContext context)
        {
            var player = new Player
            {
                Id = Guid.NewGuid(),
                Name = "Diller",
                TypePlayerId = 1
            };

            context.Players.AddOrUpdate(player);
            context.SaveChanges();
        }
    }
}
