namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStatusPropertyToPlayersTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Players", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Players", "Status");
        }
    }
}
