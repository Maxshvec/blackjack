namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChengeDataTypeRankCardPropInGamePlayersTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.GamePlayers", "RankCard", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GamePlayers", "RankCard", c => c.Int(nullable: false));
        }
    }
}
