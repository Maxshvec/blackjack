namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPropertyIsOverToGameHistoriesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GameHistories", "IsOver", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GameHistories", "IsOver");
        }
    }
}
