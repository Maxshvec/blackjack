namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteStatusProperyFromPlayersTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Players", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Players", "Status", c => c.Int(nullable: false));
        }
    }
}
