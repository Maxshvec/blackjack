namespace BlackJack.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedNullableFileToGameHistoryTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GameHistories", "WinnerId", "dbo.Players");
            DropIndex("dbo.GameHistories", new[] { "WinnerId" });
            AlterColumn("dbo.GameHistories", "WinnerId", c => c.Guid());
            CreateIndex("dbo.GameHistories", "WinnerId");
            AddForeignKey("dbo.GameHistories", "WinnerId", "dbo.Players", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GameHistories", "WinnerId", "dbo.Players");
            DropIndex("dbo.GameHistories", new[] { "WinnerId" });
            AlterColumn("dbo.GameHistories", "WinnerId", c => c.Guid(nullable: false));
            CreateIndex("dbo.GameHistories", "WinnerId");
            AddForeignKey("dbo.GameHistories", "WinnerId", "dbo.Players", "Id", cascadeDelete: true);
        }
    }
}
