﻿using BlackJack.ViewModels.ViewModels.GameController;
using BlackJack.ViewModels.ViewModels.GameController.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Logic.Interfaces
{
    public interface IGameService
    {
        Task<Guid> StartGameAsync(string name, int amount);

        Task<ResponseTakeGameView> TakeDistributionAsync(Guid gameId);

        Task<List<PlayerGetUserGameViewItem>> GetPlayer();

        Task<List<RoundGetGameGameViewItem>> GetGameCard(Guid gameHistoryId);

        Task<List<GamePlayerEnoughGameViewItem>> CompleteGame(Guid gameId);

        Task<bool> ContinueGameAsync(Guid gameId);

        Task<List<GameHistoryGetGameHistoriesGameViewItem>> GetGameHistoriesAsync();

        Task<GetGameDetailsGameView> GetGameDetailsAsync(Guid gameId);

        Task<GameDetailsGameView> GetGameById(Guid gameId);

        Task CreateUser(string name);
    }
}
