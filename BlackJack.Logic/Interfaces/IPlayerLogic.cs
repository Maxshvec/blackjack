﻿using BlackJack.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Logic.Interfaces
{
    public interface IPlayerLogic
    {
        Task<Player> CreatePlayerAsync(string name);

        Task<List<Player>> GetComputerPlayersAsync(int amount);

        Task CreateWinnerAsync(Guid playerId, Guid gameId);
    }
}
