﻿using BlackJack.Core.Entities;
using System;
using System.Threading.Tasks;

namespace BlackJack.Logic.Interfaces
{
    public interface IGameLogic
    {
        Task<GamePlayer> CreateGamePlayerAsync(Guid playerId, Guid currentRoundId);

        Task<GameHistory> CreateGameHistoryAsync(int amount);

        Task<Round> CreateRoundAsync(Guid gameId);
    }
}
