﻿using BlackJack.Data.Repositories;
using Ninject.Modules;
using BlackJack.Data.Interfaces;
using BlackJack.Data.EF;
using BlackJack.Data.Interfaces.Repositories;
using BlackJack.Logic.Interfaces;
using BlackJack.Logic.Card;

namespace BlackJack.Logic.Infrastucture
{
    public class ServiceModule : NinjectModule
    {
        private string _connectionString;

        public ServiceModule(string connectionString)
        {
            _connectionString = connectionString;
        }

        public override void Load()
        {
            Bind(typeof(IGameHistoryRepository)).To(typeof(GameHistoryRepository));
            Bind(typeof(IWinnerRepository)).To(typeof(WinnerRepository));
            Bind(typeof(IPlayerRepository)).To(typeof(PlayerRepository));
            Bind(typeof(IRoundRepository)).To(typeof(RoundRepository));
            Bind(typeof(IGamePlayerRepository)).To(typeof(GamePlayerRepository));
            Bind(typeof(IGameLogic)).To(typeof(GameLogicService));
            Bind(typeof(IPlayerLogic)).To(typeof(PlayerLogicService));
            Bind(typeof(IDataContext)).To(typeof(DataContext)).WithConstructorArgument(_connectionString);
        }
    }
}
