﻿using AutoMapper;
using BlackJack.Core.Entities;
using BlackJack.ViewModels.ViewModels.GameController;
using BlackJack.ViewModels.ViewModels.GameController.Response;
using System.Linq;

namespace BlackJack.Logic.Util
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Round, RoundGetGameGameViewItem>().ReverseMap();
                cfg.CreateMap<GamePlayer, GamePlayerGetGameGameViewItem>().ReverseMap();

                cfg.CreateMap<Round, ResponseTakeGameView>().ReverseMap();
                cfg.CreateMap<GamePlayer, GamePlayerTakeGameViewItem>().ReverseMap();

                cfg.CreateMap<GamePlayer, GamePlayerGetGameDetailsGameViewItem>().ReverseMap();
                cfg.CreateMap<GamePlayer, GamePlayerEnoughGameViewItem>().ReverseMap();

                cfg.CreateMap<Player, PlayerGetUserGameViewItem>().ReverseMap();

                cfg.CreateMap<GameHistory, GameHistoryGetGameHistoriesGameViewItem>().ForMember(s => s.Name, opt => opt.MapFrom(s => s.Players.Select(k => k.Name).FirstOrDefault()));

                cfg.CreateMap<GameHistory, GameDetailsGameView>().ReverseMap();

                cfg.CreateMap<Round, RoundGetGameDetailsViewItem>().ReverseMap();
                cfg.CreateMap<Winner, WinnerGetGameDetailsGameViewItem>().ReverseMap();

            });
        }
    }
}
