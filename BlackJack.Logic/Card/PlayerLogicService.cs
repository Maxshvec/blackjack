﻿using BlackJack.Core.Entities;
using BlackJack.Data.Interfaces.Repositories;
using BlackJack.Entitie.Enums;
using BlackJack.Logic.Infrastucture;
using BlackJack.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.Logic.Card
{
    public class PlayerLogicService : IPlayerLogic
    {
        private IPlayerRepository _playerRepository;
        private IWinnerRepository _winnerRepository;

        public PlayerLogicService(IPlayerRepository playerRepository, IWinnerRepository winnerRepository)
        {
            _winnerRepository = winnerRepository;
            _playerRepository = playerRepository;
        }

        public async Task<Player> CreatePlayerAsync(string name)
        {
            Player player = new Player
            {
                Name = name,
                TypePlayerId = (int)TypePlayers.Player
            };

            Guid result = await _playerRepository.AddAsync(player);

            if (result == Guid.Empty)
            {
                throw new ValidationException("Player can't be created", "");
            }

            return player;
        }

        public async Task<List<Player>> GetComputerPlayersAsync(int amount)
        {
            IEnumerable<Player> players = await _playerRepository.GetBotsAsync(amount);

            List<Player> listPlayers = players.ToList();

            listPlayers.Add(await _playerRepository.GetDillerAsync());

            return listPlayers;
        }

        public async Task CreateWinnerAsync(Guid playerId, Guid gameId)
        {
            Winner winner = new Winner
            {
                PlayerId = playerId,
                GameHistoryId = gameId
            };

            Guid result = await _winnerRepository.AddAsync(winner);

            if (result == Guid.Empty)
            {
                throw new ValidationException("Winner can't be created", "");
            }
        }
    }
}
