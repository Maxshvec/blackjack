﻿using BlackJack.Core.Entities;
using BlackJack.Core.Enums;
using BlackJack.Data.Interfaces.Repositories;
using BlackJack.Entitie.Enums;
using BlackJack.Logic.Infrastucture;
using BlackJack.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.Logic.Card
{
    public class GameLogicService : DeckService, IGameLogic
    {
        private IRoundRepository _roundRepository;
        private IPlayerRepository _playerRepository;
        private IGamePlayerRepository _gamePlayerRepository;
        private IGameHistoryRepository _gameHistoryRepository;
        private IPlayerLogic _playerLogic;

        public GameLogicService(IRoundRepository roundRepository, IGameHistoryRepository gameHistory,
            IGamePlayerRepository gamePlayerRepository, IPlayerRepository playerRepository, IPlayerLogic playerLogic) : base()
        {
            _roundRepository = roundRepository;
            _gameHistoryRepository = gameHistory;
            _gamePlayerRepository = gamePlayerRepository;
            _playerRepository = playerRepository;
            _playerLogic = playerLogic;
        }

        public async Task<GamePlayer> CreateGamePlayerAsync(Guid playerId, Guid currentRoundId)
        {
            CardService currentCard = TakeCard();

            Round round = await _roundRepository.GetAsync(currentRoundId);

            int playerScore = await GetPlayerScoreAsync(currentCard.Rank.ToString(), playerId, round);

            var gamePlayer = new GamePlayer
            {
                PlayerId = playerId,
                RankCard = currentCard.Rank.ToString(),
                SuitCard = (int)currentCard.Suit,
                RoundId = currentRoundId,
                Score = playerScore,
                Status = await GetPlayerStatusAsync(playerId, playerScore, round)
            };

            Guid result = await _gamePlayerRepository.AddAsync(gamePlayer);

            if (result == Guid.Empty)
            {
                throw new ValidationException("GamePlayer can't be created", "");
            }

            return gamePlayer;
        }

        public async Task<int> GetPlayerStatusAsync(Guid playerId, int playerScore, Round round)
        {
            Player diller = await _playerRepository.GetDillerAsync();

            if (diller.Id == playerId && playerScore == 21)
            {
                await _playerLogic.CreateWinnerAsync(playerId, round.GameHistoryId);

                return (int)Status.Win;
            }

            if (diller.Id == playerId && playerScore >= 17)
            {
                return (int)Status.Lost;
            }

            if (playerScore == 21 && round.RoundNumber == 1 | playerScore == 21)
            {
                await _playerLogic.CreateWinnerAsync(playerId, round.GameHistoryId);

                return (int)Status.Win;
            }

            if (playerScore > 21)
            {
                return (int)Status.Lost;
            }

            return (int)Status.InGame;
        }

        public async Task<Round> CreateRoundAsync(Guid gameId)
        {
            Round round = await _roundRepository.GetLastRoundByGameIdAsync(gameId);

            if (round == null)
            {
                round = new Round
                {
                    RoundNumber = -1
                };
            }

            var currentRound = new Round
            {
                GameHistoryId = gameId,
                RoundNumber = round.RoundNumber + 1,
                GamePlayers = new List<GamePlayer>()
            };

            Guid result = await _roundRepository.AddAsync(currentRound);

            if (result == Guid.Empty)
            {
                throw new ValidationException("Round can't be created", "");
            }

            return currentRound;
        }

        public async Task<GameHistory> CreateGameHistoryAsync(int amount)
        {
            var currentGame = new GameHistory
            {
                CountPlayer = amount + 2,
                IsOver = false
            };

            Guid result = await _gameHistoryRepository.AddAsync(currentGame);

            if (result == Guid.Empty)
            {
                throw new ValidationException("GameHistory can't be created", "");
            }
            
            return currentGame;
        }
        
        private async Task<int> GetPlayerScoreAsync(string cardName, Guid playerId, Round round)
        {
            int result = 0;

            int value = (int)(WeightCard)Enum.Parse(typeof(WeightCard), cardName);

            if (round.RoundNumber == 0)
            {
                return result += value;
            }

            Round lastRound = await _roundRepository.GetPreviousRoundByGameIdAsync(round.GameHistoryId);
            GamePlayer gamePlayer = await _gamePlayerRepository.GetGamePlayerByRoundIdAsync(playerId, lastRound.Id);

            if (gamePlayer != null)
            {
                result = gamePlayer.Score;
            }

            if (result >= 11 && value == 11)
            {
                result += 1;
                return result;
            }

            return result += value;
        }
    }
}