﻿using BlackJack.Entitie.Enums;
using System;

namespace BlackJack.Logic.Card
{
    public class CardService
    {
        public RankCard Rank { get; private set; }
        public SuitCard Suit { get; private set; }

        public CardService(RankCard rankOfCard, SuitCard suitOfCard)
        {
            Rank = rankOfCard;
            Suit = suitOfCard;
        }

        public static int NumberRanks()
        {
            return Enum.GetValues(typeof(RankCard)).Length - 1;
        }

        public static int NumberSuits()
        {
            return Enum.GetValues(typeof(SuitCard)).Length - 1;
        }
        
        public static CardService GetTheSameCard(CardService selectedCard)
        {
            return new CardService(selectedCard.Rank, selectedCard.Suit);
        }

        public static CardService GetDummyCard()
        {
            var card = new CardService(RankCard.Dummy, SuitCard.Dummy);

            return card;
        }
    }
}
