﻿using BlackJack.Entitie.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackJack.Logic.Card
{
    public class DeckService
    {
        private CardService[] _spades;
        private CardService[] _clubs;
        private CardService[] _hearts;
        private CardService[] _diamonds;

        protected Random _random = new Random();
        
        public List<CardService> Cards { get; private set; } = new List<CardService>();
        
        //Change static field
        public DeckService()
        {
            _spades = GetCardsArray(SuitCard.Spades);
            _clubs = GetCardsArray(SuitCard.Clubs);
            _hearts = GetCardsArray(SuitCard.Hearts);
            _diamonds = GetCardsArray(SuitCard.Diamonds);

            SetRandomOrder();
        }

        private CardService[] GetCardsArray(SuitCard suitOfCard)
        {
            List<CardService> listCards = new List<CardService>();

            foreach (RankCard rank in Enum.GetValues(typeof(RankCard)))
            {
                listCards.Add(new CardService(rank, suitOfCard));
            }

            return listCards.ToArray();
        }

        //Mix deck
        public void SetRandomOrder()
        {
            SetListCards();

            int[] randomIndexes = Enumerable.Range(0, InitialNumberCards())
                                        .OrderBy(n => _random.Next(0, CardService.NumberRanks()))
                                        .ToArray();

            List<CardService> newList = new List<CardService>();

            foreach(var index in randomIndexes)
            {
                newList.Add(Cards[index]);
            }

            Cards = newList;
        }
        
        public CardService TakeCard()
        {
            List<CardService> notDummyCards = Cards
                .Where(c => c.Rank != RankCard.Dummy)
                .ToList();

            if (notDummyCards.Count == 0)
            {
                throw new Exception("Deck is over");
            }

            int index = _random.Next(0, notDummyCards.Count());

            CardService selectedCard = notDummyCards[index];
            
            index = Cards.IndexOf(selectedCard);

            Cards[index] = CardService.GetDummyCard();

            return selectedCard;
        }

        private void SetListCards()
        {
            Cards.AddRange(_clubs);
            Cards.AddRange(_diamonds);
            Cards.AddRange(_hearts);
            Cards.AddRange(_spades);
        }

        private int InitialNumberCards()
        {
            return CardService.NumberRanks() * CardService.NumberSuits();
        }
    }
}
