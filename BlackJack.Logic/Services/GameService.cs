﻿using AutoMapper;
using BlackJack.Core.Entities;
using BlackJack.Data.Interfaces.Repositories;
using BlackJack.Entitie.Enums;
using BlackJack.Logic.Infrastucture;
using BlackJack.Logic.Interfaces;
using BlackJack.ViewModels.ViewModels.GameController;
using BlackJack.ViewModels.ViewModels.GameController.Enum;
using BlackJack.ViewModels.ViewModels.GameController.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.Logic.Services
{
    public class GameService : IGameService
    {
        private IGameLogic _gameLogic;
        private IPlayerLogic _playerLogic;
        private IPlayerRepository _playerRepository;
        private IRoundRepository _roundRepository;
        private IGamePlayerRepository _gamePlayerRepository;
        private IWinnerRepository _winnerRepository;
        private IGameHistoryRepository _gameHistoryRepository;

        public GameService(IGameLogic gameLogic, IPlayerLogic gamePlayer, IPlayerRepository playerRepository, IRoundRepository roundRepository,
            IGamePlayerRepository gamePlayerRepository, IWinnerRepository winnerRepository, IGameHistoryRepository gameHistoryRepository)
        {
            _gameLogic = gameLogic;
            _playerLogic = gamePlayer;
            _playerRepository = playerRepository;
            _roundRepository = roundRepository;
            _gamePlayerRepository = gamePlayerRepository;
            _winnerRepository = winnerRepository;
            _gameHistoryRepository = gameHistoryRepository;
        }

        public async Task<Guid> StartGameAsync(string name, int amount)
        {
            GameHistory currentGame = await _gameLogic.CreateGameHistoryAsync(amount);

            Player player = await _playerRepository.FindPlayerByName(name);

            List<Player> computerPlayers = await _playerLogic.GetComputerPlayersAsync(amount);

            computerPlayers.Add(player);

            Round currentRound = await _gameLogic.CreateRoundAsync(currentGame.Id);

            foreach (Player item in computerPlayers)
            {
                await _gameLogic.CreateGamePlayerAsync(item.Id, currentRound.Id);
            }

            await TakeDistributionAsync(currentGame.Id);

            return currentGame.Id;
        }

        public async Task<ResponseTakeGameView> TakeDistributionAsync(Guid gameId)
        {
            IEnumerable<GamePlayer> gamePlayers = await _gamePlayerRepository.GetGamePlayersInLastRoundByGame(gameId);

            Round currentRound = new Round();

            if (gamePlayers.Any())
            {
                currentRound = await _gameLogic.CreateRoundAsync(gameId);

                foreach (GamePlayer gamePlayer in gamePlayers)
                {
                    GamePlayer player = await _gameLogic.CreateGamePlayerAsync(gamePlayer.PlayerId, currentRound.Id);

                    currentRound.GamePlayers.Add(player);
                }
            }

            ResponseTakeGameView roundViewModel = Mapper.Map<Round, ResponseTakeGameView>(currentRound);

            foreach (GamePlayerTakeGameViewItem gamePlayer in roundViewModel.GamePlayers)
            {
                Player player = await _playerRepository.GetPlayerById(gamePlayer.PlayerId);

                gamePlayer.Name = player.Name;
                gamePlayer.Suit = Enum.GetName(typeof(SuitCard), gamePlayer.SuitCard);
                gamePlayer.RankCard = GetCard(gamePlayer.RankCard);

                gamePlayer.IsUser = GetUserFlag(player);
            }

            bool result = await ContinueGameAsync(gameId);

            if (result == false)
            {
                roundViewModel.GameStatus = GameStatus.Finished;
            }

            if (result == true)
            {
                roundViewModel.GameStatus = GameStatus.InProcess;
            }

            return roundViewModel;
        }

        public async Task<bool> ContinueGameAsync(Guid gameId)
        {
            GamePlayer user = await _gamePlayerRepository.GetUserByGameInLastRound(gameId);

            ICollection<GamePlayer> players = (ICollection<GamePlayer>)await _gamePlayerRepository.GetGamePlayersInLastRoundByGame(gameId);

            GameHistory gameHistory = await _gameHistoryRepository.GetAsync(gameId);

            if (user != null && gameHistory.IsOver == false && players.Count > 1)
            {
                return true;
            }

            return false;
        }

        public async Task<List<GameHistoryGetGameHistoriesGameViewItem>> GetGameHistoriesAsync()
        {
            List<GameHistory> gameHistory = await _gameHistoryRepository.GetGameHistory();

            List<GameHistoryGetGameHistoriesGameViewItem> gameHistoryViewModels = Mapper.Map<List<GameHistory>, List<GameHistoryGetGameHistoriesGameViewItem>>(gameHistory);

            return gameHistoryViewModels;
        }

        public async Task<GetGameDetailsGameView> GetGameDetailsAsync(Guid gameId)
        {
            List<Round> rounds = await _roundRepository.GetGameDetails(gameId);

            rounds.LastOrDefault().Winners = await _winnerRepository.GetWinnersByGameIdAsync(gameId);

            List<RoundGetGameDetailsViewItem> roundsWithDetailsViewModel = Mapper.Map<List<Round>, List<RoundGetGameDetailsViewItem>>(rounds);

            foreach (RoundGetGameDetailsViewItem round in roundsWithDetailsViewModel)
            {
                round.RoundNumber += 1;

                foreach (GamePlayerGetGameDetailsGameViewItem gamePlayer in round.GamePlayers)
                {
                    Player player = await _playerRepository.GetPlayerById(gamePlayer.PlayerId);

                    gamePlayer.PlayerName = player.Name;
                    gamePlayer.Suit = Enum.GetName(typeof(SuitCard), gamePlayer.SuitCard);
                    gamePlayer.RankCard = GetCard(gamePlayer.RankCard);
                }

                foreach (WinnerGetGameDetailsGameViewItem winner in round.Winners)
                {
                    Player player = await _playerRepository.GetPlayerById(winner.PlayerId);

                    winner.PlayerName = player.Name;
                }
            }
            GameHistory game = await _gameHistoryRepository.GetAsync(gameId);

            GetGameDetailsGameView gameDetails = new GetGameDetailsGameView
            {
                isOver = game.IsOver,
                Rounds = roundsWithDetailsViewModel
            };

            return gameDetails;
        }

        public async Task<List<RoundGetGameGameViewItem>> GetGameCard(Guid gameHistoryId)
        {
            List<Round> rounds = await _roundRepository.GetRoundsByGameIdAsync(gameHistoryId);

            List<RoundGetGameGameViewItem> roundViewModel = Mapper.Map<List<Round>, List<RoundGetGameGameViewItem>>(rounds);

            foreach (RoundGetGameGameViewItem item in roundViewModel)
            {
                foreach (GamePlayerGetGameGameViewItem gamePlayer in item.GamePlayers)
                {
                    Player player = await _playerRepository.GetAsync(gamePlayer.PlayerId);
                    gamePlayer.Name = player.Name;
                    gamePlayer.Suit = Enum.GetName(typeof(SuitCard), gamePlayer.SuitCard);

                    gamePlayer.IsUser = GetUserFlag(player);

                    gamePlayer.RankCard = GetCard(gamePlayer.RankCard);
                }
            }

            return roundViewModel;
        }

        public string GetCard(string cardName)
        {
            int value = (int)(RankCard)Enum.Parse(typeof(RankCard), cardName);

            if (value <= 10)
            {
                return value.ToString();
            }

            return cardName;
        }

        public async Task<List<GamePlayerEnoughGameViewItem>> CompleteGame(Guid gameId)
        {
            IEnumerable<Round> rounds = await _roundRepository.GetRoundsByGameIdAsync(gameId);

            List<GamePlayer> winners = new List<GamePlayer>();

            foreach (Round round in rounds)
            {
                winners.AddRange(await _gamePlayerRepository.GetWinnersAsync(round.Id));
            }

            if (!winners.Any())
            {
                Round lastRound = await _roundRepository.GetLastRoundByGameIdAsync(gameId);

                IEnumerable<GamePlayer> gamePlayers = await _gamePlayerRepository.GetPlayersInGameByRoundIdAsync(lastRound.Id);

                List<GamePlayer> winner = gamePlayers
                    .Where(q => q.Score == gamePlayers.Max(z => z.Score))
                    .ToList();

                winners.AddRange(winner);
            }

            GameHistory game = await _gameHistoryRepository.GetAsync(gameId);

            if (game.IsOver == false)
            {
                foreach (GamePlayer gamePlayer in winners)
                {
                    Winner winner = new Winner
                    {
                        GameHistoryId = gameId,
                        PlayerId = gamePlayer.PlayerId
                    };

                    Guid result = await _winnerRepository.AddAsync(winner);

                    if (result == Guid.Empty)
                    {
                        throw new ValidationException("Winner can't created", "");
                    }
                }
            }

            List<GamePlayerEnoughGameViewItem> gamePlayerViewModel = Mapper.Map<List<GamePlayer>, List<GamePlayerEnoughGameViewItem>>(winners);

            foreach (GamePlayerEnoughGameViewItem gamePlayer in gamePlayerViewModel)
            {
                Player player = await _playerRepository.GetAsync(gamePlayer.PlayerId);
                gamePlayer.Name = player.Name;
            }

            GameHistory gameHistory = await _gameHistoryRepository.GetAsync(gameId);

            gameHistory.IsOver = true;

            bool updateResult = await _gameHistoryRepository.UpdateAsync(gameHistory);

            if (updateResult == false)
            {
                throw new ValidationException("Game can't be completed", "");
            }

            return gamePlayerViewModel;
        }

        public async Task<List<PlayerGetUserGameViewItem>> GetPlayer()
        {
            return Mapper.Map<IEnumerable<Player>, List<PlayerGetUserGameViewItem>>(await _playerRepository.GetPlayersAsync());
        }

        public async Task CreateUser(string name)
        {
            Player newPlayer = new Player
            {
                Name = name,
                TypePlayerId = 0
            };

            Guid player = await _playerRepository.AddAsync(newPlayer);

            if (player == Guid.Empty)
            {
                throw new ValidationException("Player can't created", "");
            }
        }

        public async Task<GameDetailsGameView> GetGameById(Guid gameId)
        {
            GameHistory game = await _gameHistoryRepository.GetAsync(gameId);

            return Mapper.Map<GameHistory, GameDetailsGameView>(game);
        }

        private bool GetUserFlag(Player player)
        {
            if (player.TypePlayerId == (int)TypePlayers.Player)
            {
                return true;
            }

            return false;
        }
    }
}
