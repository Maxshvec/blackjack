﻿namespace BlackJack.ViewModels.ViewModels.GameController.Enum
{
    public enum GameStatus
    {
        InProcess,
        Finished
    }
}
