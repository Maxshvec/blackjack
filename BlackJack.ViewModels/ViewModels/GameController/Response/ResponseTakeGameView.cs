﻿using BlackJack.ViewModels.ViewModels.GameController.Enum;
using System;
using System.Collections.Generic;

namespace BlackJack.ViewModels.ViewModels.GameController.Response
{
    public class ResponseTakeGameView
    {
        public int RoundNumber { get; set; }

        public GameStatus GameStatus { get; set; }

        public List<GamePlayerTakeGameViewItem> GamePlayers { get; set; }
    }

    public class GamePlayerTakeGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string Name { get; set; }

        public bool IsUser { get; set; }

        public string RankCard { get; set; }

        public int SuitCard { get; set; }

        public string Suit { get; set; }

        public int Score { get; set; }
    }
}
