﻿using BlackJack.ViewModels.ViewModels.GameController.Enum;
using System;
using System.Collections.Generic;

namespace BlackJack.ViewModels.ViewModels.GameController
{
    public class GetGameGameView
    {
        public List<RoundGetGameGameViewItem> Rounds { get; set; }
    }

    public class RoundGetGameGameViewItem
    {
        public int RoundNumber { get; set; }

        public GameStatus GameStatus { get; set; }

        public List<GamePlayerGetGameGameViewItem> GamePlayers { get; set; }
    }

    public class GamePlayerGetGameGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string Name { get; set; }

        public bool IsUser { get; set; }

        public string RankCard { get; set; }

        public int SuitCard { get; set; }

        public string Suit { get; set; }

        public int Score { get; set; }
    }
}
