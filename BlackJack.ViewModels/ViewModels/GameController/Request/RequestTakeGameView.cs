﻿namespace BlackJack.ViewModels.ViewModels.GameController.Request
{
    public class RequestTakeGameView
    {
        public string Id { get; set; }
    }
}
