﻿using System;
using System.Collections.Generic;

namespace BlackJack.ViewModels.ViewModels.GameController
{
    public class GetGameDetailsGameView
    {
        public bool isOver { get; set; }

        public List<RoundGetGameDetailsViewItem> Rounds { get; set; }
    }

    public class RoundGetGameDetailsViewItem
    {
        public int RoundNumber { get; set; }

        public Guid GameHistoryId { get; set; }

        public List<GamePlayerGetGameDetailsGameViewItem> GamePlayers { get; set; }

        public List<WinnerGetGameDetailsGameViewItem> Winners { get; set; }
    }

    public class GamePlayerGetGameDetailsGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string PlayerName { get; set; }
        
        public string RankCard { get; set; }

        public int SuitCard { get; set; }

        public string Suit { get; set; }

        public string Score { get; set; }

        public int Status { get; set; }
    }

    public class WinnerGetGameDetailsGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string PlayerName { get; set; }
    }

}
