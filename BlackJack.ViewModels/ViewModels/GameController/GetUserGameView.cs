﻿using System.Collections.Generic;

namespace BlackJack.ViewModels.ViewModels.GameController
{
    public class GetUserGameView
    {
        public List<PlayerGetUserGameViewItem> Players { get; set; }
    }

    public class PlayerGetUserGameViewItem
    {
        public string Name { get; set; }
    }
}
