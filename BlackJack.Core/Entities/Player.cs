﻿using BlackJack.Core.Entities.Base;
using Dapper.Contrib.Extensions;

namespace BlackJack.Core.Entities
{
    [Table("Players")]
    public class Player : EntityBase
    {
        public string Name { get; set; }

        public int TypePlayerId { get; set; }
    }
}
