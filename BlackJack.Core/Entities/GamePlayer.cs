﻿using BlackJack.Core.Entities.Base;
using Dapper.Contrib.Extensions;
using System;

namespace BlackJack.Core.Entities
{
    [Table("GamePlayers")]
    public class GamePlayer : EntityBase
    {
        public Guid PlayerId { get; set; }

        public Guid RoundId { get; set; }

        public string RankCard { get; set; }
        
        public int SuitCard { get; set; }

        public int Score { get; set; }

        public int Status { get; set; }
    }
}
