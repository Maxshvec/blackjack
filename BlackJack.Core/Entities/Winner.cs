﻿using BlackJack.Core.Entities.Base;
using System;
using Dapper.Contrib.Extensions;

namespace BlackJack.Core.Entities
{
    [Table("Winners")]
    public class Winner : EntityBase
    {
        public Guid PlayerId { get; set; }

        public Guid GameHistoryId { get; set; }
    }
}
