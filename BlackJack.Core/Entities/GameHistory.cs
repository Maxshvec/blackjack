﻿using BlackJack.Core.Entities.Base;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BlackJack.Core.Entities
{
    [Table("GameHistories")]
    public class GameHistory : EntityBase
    {
        public int CountPlayer { get; set; }
        
        [Computed]
        public List<Player> Players { get; set; }

        public bool IsOver { get; set; }
    }
}
