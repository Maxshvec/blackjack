﻿using BlackJack.Core.Entities.Base;
using System;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;

namespace BlackJack.Core.Entities
{
    [Table("Rounds")]
    public class Round : EntityBase
    {
        public int RoundNumber { get; set; }
        
        public Guid GameHistoryId { get; set; }

        [Computed]
        public List<GamePlayer> GamePlayers { get; set; }

        [Computed]
        public List<Winner> Winners { get; set; }
    }
}
