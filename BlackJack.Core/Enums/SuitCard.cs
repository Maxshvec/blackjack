﻿namespace BlackJack.Entitie.Enums
{
    public enum SuitCard
    {
        Spades = 0, 
        Clubs = 1,  
        Hearts = 2, 
        Diamonds = 3,
        Dummy,
    }
}
